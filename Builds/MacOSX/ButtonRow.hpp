//
//  ButtonRow.hpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 27/10/2016.
//
//

#ifndef ButtonRow_hpp
#define ButtonRow_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include "ScottButton.hpp"

class ButtonRow :public Component

{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
    
private:
    ScottButton button[5];
    
};

#endif /* ButtonRow_hpp */
