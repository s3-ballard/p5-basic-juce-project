//
//  ScottButton.cpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 25/10/2016.
//
//

#include "ScottButton.hpp"

ScottButton::ScottButton()
{
    
}
ScottButton::~ScottButton()
{
    
}

void ScottButton:: paint(Graphics& g)
{
    //g.setColour(Colours::whitesmoke);;
    
    
    if (isMouseIn == true)
    {
        g.setColour(Colours::aliceblue);
    }
    
    else if (isMouseIn == false)
    {
        g.setColour(Colours::whitesmoke);
    }
    if (isMouseDown == true && isMouseIn == true)
    {
        g.setColour(Colours::red);
    }
 
    
    g.drawRect(getLocalBounds());
    g.fillRect(getLocalBounds());
}

void ScottButton::mouseEnter (const MouseEvent& event)
{
    DBG("mouse entered");
    
    setColour(1, Colours::aliceblue);
    isMouseIn = true;
    repaint();
    
    
}
void ScottButton::mouseDown(const MouseEvent& event)
{
    DBG("mouse down");
    
    isMouseDown = true;
    repaint();
}



void ScottButton::mouseUp(const MouseEvent& event)
{
    DBG("mouse up");
    isMouseDown = false;
    repaint();
}

void ScottButton:: mouseExit(const MouseEvent& event)
{
    DBG("mouse exit");
    isMouseIn = false;
    repaint();
}

