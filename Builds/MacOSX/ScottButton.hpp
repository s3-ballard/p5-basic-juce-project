//
//  ScottButton.hpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 25/10/2016.
//
//

#ifndef ScottButton_hpp
#define ScottButton_hpp
#include "../JuceLibraryCode/JuceHeader.h"

#include <stdio.h>

class ScottButton   : public Component

{
public:
    ScottButton();
    ~ScottButton();
    
    void paint (Graphics& g) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    
    
private:
    Component button;
    bool isMouseIn;
    bool isMouseDown;
};

#endif /* ScottButton_hpp */
