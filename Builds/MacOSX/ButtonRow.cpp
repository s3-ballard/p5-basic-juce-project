//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 27/10/2016.
//
//

#include "ButtonRow.hpp"

ButtonRow::ButtonRow()
{
    for (int counter = 0; counter < 5; counter++)
    {
    addAndMakeVisible(button[counter]);
    }
    
}

ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::resized()
{
    button[0].setBounds(0, 50, getWidth() * 0.2, getHeight());
    button[1].setBounds(getWidth() * 0.2, 50, getWidth() * 0.2, getHeight());
    button[2].setBounds(getWidth() * 0.4, 50, getWidth() * 0.2, getHeight());
    button[3].setBounds(getWidth() * 0.6, 50, getWidth() * 0.2, getHeight());
    button[4].setBounds(getWidth() * 0.8, 50, getWidth() * 0.2, getHeight());
}
